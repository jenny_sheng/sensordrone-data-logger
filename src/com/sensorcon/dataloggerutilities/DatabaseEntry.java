package com.sensorcon.dataloggerutilities;

import com.j256.ormlite.field.DatabaseField;

public class DatabaseEntry {
	
	@DatabaseField(generatedId = true)
	int id;
	@DatabaseField(index = true)
	long timeStamp;
	@DatabaseField
	int sensor;
	@DatabaseField
	float value;
	@DatabaseField
	String MAC;
	
	DatabaseEntry() {}
	
	public DatabaseEntry(long timeStamp, int sensor, float value, String MAC) {
		this.timeStamp = timeStamp;
		this.sensor = sensor;
		this.value = value;
		this.MAC = MAC;
	}
	
	public long getTimeStamp() {
		return timeStamp;
	}
	
	public int getSensor() {
		return sensor;
	}
	
	public float getValue() {
		return value;
	}
	
	public String getMAC() {
		return MAC;
	}
	
	public String toString() {
		String s = "";
		s += sensor + " " + value;
		return s;
	}
}
