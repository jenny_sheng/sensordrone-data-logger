package com.sensorcon.dataloggerutilities;

import com.j256.ormlite.field.DatabaseField;

public class MACEntry {
	
	@DatabaseField(generatedId = true)
	int id;
	@DatabaseField(index = true)
	String MAC;
	
	public MACEntry() {}
	
	public MACEntry(String MAC) {
		this.MAC = MAC;
	}
	
	public String getMAC() {
		return MAC;
	}
}
