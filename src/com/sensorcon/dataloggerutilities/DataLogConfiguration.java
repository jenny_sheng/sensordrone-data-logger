package com.sensorcon.dataloggerutilities;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.os.Handler;

public class DataLogConfiguration {
	
	public final static int NUM_SENSORS = 14;
	
	public static boolean sensorsEnabled[];
	public static int sampleRate = 0;
	public static long totalTime = 0;
	public static long totalLogs = 0;
	public static long timeStamp;
	public static String macAddress;
	public static float batteryVoltage;
	public static ArrayList<byte[]> entryList;
	public static float coBaseline;
	public static float coSensitivity;
	public static boolean usingProfile = false;
	
	public DataLogConfiguration() {
		sensorsEnabled = new boolean[NUM_SENSORS];
		for(int i = 0; i < NUM_SENSORS; i++) {
			sensorsEnabled[i] = false;
		}
		
		sampleRate = 0;
		totalTime = 0;
		totalLogs = 0;
		timeStamp = 0;
		macAddress = "";
		batteryVoltage = 0;
	}
}
