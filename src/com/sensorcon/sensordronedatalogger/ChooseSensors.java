package com.sensorcon.sensordronedatalogger;

import com.sensorcon.dataloggerutilities.DataLogConfiguration;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;

/**
 * Lets user choose sensors to log
 * 
 * @author Sensorcon, Inc.
 * @version 1.0.0 
 */
public class ChooseSensors extends Activity {
	
	private ImageButton btnBack;
	private ImageButton btnNext;
	private CheckBox cbTemperature;
	private CheckBox cbHumidity;
	private CheckBox cbPressure;
	private CheckBox cbPrecisionGas;
	private CheckBox cbReducingGas;
	private CheckBox cbOxidizingGas;
	private CheckBox cbIRTemperature;
	private CheckBox cbCapacitance;
	private CheckBox cbExADC;
	private CheckBox cbRed;
	private CheckBox cbGreen;
	private CheckBox cbBlue;
	private CheckBox cbClear;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_sensors);
		
		btnBack = (ImageButton)findViewById(R.id.btnBack);
		btnNext = (ImageButton)findViewById(R.id.btnNext);
		cbTemperature = (CheckBox)findViewById(R.id.checkbox_temp);
		cbHumidity = (CheckBox)findViewById(R.id.checkbox_humidity);
		cbPressure = (CheckBox)findViewById(R.id.checkbox_pressure);
		cbPrecisionGas = (CheckBox)findViewById(R.id.checkbox_gas);
		cbReducingGas = (CheckBox)findViewById(R.id.checkbox_red);
		cbOxidizingGas = (CheckBox)findViewById(R.id.checkbox_ox);
		cbIRTemperature = (CheckBox)findViewById(R.id.checkbox_ir);
		cbCapacitance = (CheckBox)findViewById(R.id.checkbox_cap);
		cbExADC = (CheckBox)findViewById(R.id.checkbox_adc);
		cbRed = (CheckBox)findViewById(R.id.checkbox_cred);
		cbGreen = (CheckBox)findViewById(R.id.checkbox_cgreen);
		cbBlue = (CheckBox)findViewById(R.id.checkbox_cblue);
		cbClear = (CheckBox)findViewById(R.id.checkbox_cclear);
		
		btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	back();
             }
        });
		
		btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	next();
             }
        });
		
		// If back button was pressed, restore chosen sensors
		restore();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}
	
	/**
	 * Restores the chosen sensors
	 */
	private void restore() {
		if(DataLogConfiguration.sensorsEnabled[0] == true) {
			cbTemperature.setChecked(true);
		}
		if(DataLogConfiguration.sensorsEnabled[1] == true) {
			cbHumidity.setChecked(true);
		}
		if(DataLogConfiguration.sensorsEnabled[2] == true) {
			cbPressure.setChecked(true);
		}
		if(DataLogConfiguration.sensorsEnabled[3] == true) {
			cbPrecisionGas.setChecked(true);
		}
		if(DataLogConfiguration.sensorsEnabled[4] == true) {
			cbReducingGas.setChecked(true);
		}
		if(DataLogConfiguration.sensorsEnabled[5] == true) {
			cbOxidizingGas.setChecked(true);
		}
		if(DataLogConfiguration.sensorsEnabled[6] == true) {
			cbIRTemperature.setChecked(true);
		}
		if(DataLogConfiguration.sensorsEnabled[7] == true) {
			cbCapacitance.setChecked(true);
		}
		if(DataLogConfiguration.sensorsEnabled[8] == true) {
			cbExADC.setChecked(true);
		}
		if(DataLogConfiguration.sensorsEnabled[9] == true) {
			cbRed.setChecked(true);
		}
		if(DataLogConfiguration.sensorsEnabled[10] == true) {
			cbGreen.setChecked(true);
		}
		if(DataLogConfiguration.sensorsEnabled[11] == true) {
			cbBlue.setChecked(true);
		}
		if(DataLogConfiguration.sensorsEnabled[12] == true) {
			cbClear.setChecked(true);
		}
	}
	
	/**
	 * Makes sure at least one sensor was chosen
	 * 
	 * @return	true if at least one sensor chosen
	 */
	private boolean valid() {
		boolean valid = false;
		
		for(int i = 0; i < DataLogConfiguration.NUM_SENSORS; i++) {
			if(DataLogConfiguration.sensorsEnabled[i] == true) {
				valid = true;
			}
		}
		
		return valid;
	}
	
	/**
	 * Stores sensors that are chosen
	 * 
	 * @param view
	 */
	public void onCheckboxClicked(View view) {
	   
	    boolean checked = ((CheckBox) view).isChecked();
	    
	    // Check which checkbox was clicked
	    switch(view.getId()) {
	        case R.id.checkbox_temp:
	            if (checked) {
	                DataLogConfiguration.sensorsEnabled[0] = true;
	            }
	            else {
	            	DataLogConfiguration.sensorsEnabled[0] = false;
	            }
	            break;
	        case R.id.checkbox_humidity:
	        	if (checked) {
	                DataLogConfiguration.sensorsEnabled[1] = true;
		        }
	        	else {
	            	DataLogConfiguration.sensorsEnabled[1] = false;
	            }
	            break;
	        case R.id.checkbox_pressure:
	        	if (checked) {
	                DataLogConfiguration.sensorsEnabled[2] = true;
		        } 
	        	else {
	            	DataLogConfiguration.sensorsEnabled[2] = false;
	            }
	            break;
	        case R.id.checkbox_gas:
	        	if (checked) {
	                DataLogConfiguration.sensorsEnabled[3] = true;
		        } 
	        	else {
	            	DataLogConfiguration.sensorsEnabled[3] = false;
	            }
	            break;
	        case R.id.checkbox_red:
	        	if (checked) {
	                DataLogConfiguration.sensorsEnabled[4] = true;
		        } 
	        	else {
	            	DataLogConfiguration.sensorsEnabled[4] = false;
	            }
	            break;
	        case R.id.checkbox_ox:
	        	if (checked) {
	                DataLogConfiguration.sensorsEnabled[5] = true;
		        } 
	        	else {
	            	DataLogConfiguration.sensorsEnabled[5] = false;
	            }
	            break;
	        case R.id.checkbox_ir:
	        	if (checked) {
	                DataLogConfiguration.sensorsEnabled[6] = true;
		        } 
	        	else {
	            	DataLogConfiguration.sensorsEnabled[6] = false;
	            }
	            break;
	        case R.id.checkbox_cap:
	        	if (checked) {
	                DataLogConfiguration.sensorsEnabled[7] = true;
		        } 
	        	else {
	            	DataLogConfiguration.sensorsEnabled[7] = false;
	            }
	            break;
	        case R.id.checkbox_adc:
	        	if (checked) {
	                DataLogConfiguration.sensorsEnabled[8] = true;
		        } 
	        	else {
	            	DataLogConfiguration.sensorsEnabled[8] = false;
	            }
	            break;
	        case R.id.checkbox_cred:
	        	if (checked) {
	                DataLogConfiguration.sensorsEnabled[9] = true;
		        } 
	        	else {
	            	DataLogConfiguration.sensorsEnabled[9] = false;
	            }
	            break;
	        case R.id.checkbox_cgreen:
	        	if (checked) {
	                DataLogConfiguration.sensorsEnabled[10] = true;
		        } 
	        	else {
	            	DataLogConfiguration.sensorsEnabled[10] = false;
	            }
	            break;
	        case R.id.checkbox_cblue:
	        	if (checked) {
	                DataLogConfiguration.sensorsEnabled[11] = true;
		        } 
	        	else {
	            	DataLogConfiguration.sensorsEnabled[11] = false;
	            }
	            break;
	        case R.id.checkbox_cclear:
	        	if (checked) {
	                DataLogConfiguration.sensorsEnabled[12] = true;
		        } 
	        	else {
	            	DataLogConfiguration.sensorsEnabled[12] = false;
	            }
	            break;
	    }
	}
	
	/**
	 * Kills activity if back button pressed
	 */
	private void back() {
		Intent myIntent = new Intent(getApplicationContext(), Profiles.class);
		startActivity(myIntent);
		this.finish();
	}
	
	/**
	 * Kills activity
	 */
	private void kill() {
		this.finish();
	}
	
	/**
	 * Starts next activity
	 */
	private void next() {
		if(!valid()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Please choose at least one sensor.")
			       .setCancelable(false)
			       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			           }
			       });
			AlertDialog alert = builder.create();
			alert.show();
		}
		else {
			Intent myIntent = new Intent(getApplicationContext(), SampleInterval.class);
			startActivity(myIntent);
			kill();
		}
	}
}
