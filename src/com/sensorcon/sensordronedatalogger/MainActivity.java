package com.sensorcon.sensordronedatalogger;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.sensorcon.dataloggerutilities.DataLogConfiguration;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

/**
 * Main entrance to app
 * 
 * @author Sensorcon, Inc.
 * @version 1.0.0 
 */
public class MainActivity extends Activity {
	
	// GUI vars
	private ImageButton btnStartLogging;
	private ImageButton btnDownloadData;
	private ImageButton btnViewData;
	// Datalog vars
	public DataLogConfiguration config;
    private SharedPreferences preferences;
    static String DISABLE_INTRO = "DISABLE_INTRO";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        // Initialize SharedPreferences
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		
		// Initialize configuration parameters
		DataLogConfiguration.sensorsEnabled = new boolean[DataLogConfiguration.NUM_SENSORS];
		for(int i = 0; i < DataLogConfiguration.NUM_SENSORS; i++) {
			DataLogConfiguration.sensorsEnabled[i] = false;
		}
		DataLogConfiguration.sampleRate = 0;
		DataLogConfiguration.totalTime = 0;
		DataLogConfiguration.timeStamp = 0;
		
		btnStartLogging = (ImageButton)findViewById(R.id.btnStartLogging);
		btnDownloadData = (ImageButton)findViewById(R.id.btnDownloadData);
		btnViewData = (ImageButton)findViewById(R.id.btnViewData);
		
		btnStartLogging.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent myIntent = new Intent(getApplicationContext(), Connect.class);
				startActivity(myIntent);
             }
        });
		
		btnDownloadData.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent myIntent = new Intent(getApplicationContext(), Download.class);
				startActivity(myIntent);
             }
        });
		
		btnViewData.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent myIntent = new Intent(getApplicationContext(), ViewData.class);
				startActivity(myIntent);
             }
        });

        // Check if user has disabld intro message
        String disableIntro = preferences.getString(DISABLE_INTRO, "");
        if(!disableIntro.equals("DISABLE")) {
            showIntroDialog();
        }
	}

	@Override
    public void onDestroy() {
        super.onDestroy();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}

    /**
     * Loads the dialog shown at startup
     */
    public void showIntroDialog() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        alert.setTitle("Introduction").setMessage("If you are new to the Sensordrone Data Logger, you should read through the instructions. To access them, go to the main menu and select Instructions.");
        alert.setNegativeButton("Don't Show Again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor prefEditor = preferences.edit();
                prefEditor.putString(DISABLE_INTRO, "DISABLE");
                prefEditor.commit();
            }
        })
        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // do nothing
            }
        }).show();
    }
}
