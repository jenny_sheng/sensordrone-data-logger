package com.sensorcon.sensordronedatalogger;

import com.sensorcon.dataloggerutilities.DataLogConfiguration;
import com.sensorcon.sensordrone.DroneEventListener;
import com.sensorcon.sensordrone.DroneEventObject;
import com.sensorcon.sensordrone.DroneStatusListener;
import com.sensorcon.sensordrone.android.Drone;
import com.sensorcon.sensordrone.android.tools.DroneConnectionHelper;
import com.sensorcon.sensordrone.android.tools.DroneStreamer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * Connects to drone, stores MAC address and battery voltage
 * 
 * @author Sensorcon, Inc.
 */
public class Connect extends Activity {
	
	private ImageButton btnFindSensordrone;
	private ImageButton btnBack;
	
	private Drone myDrone;
	private DroneConnectionHelper droneHelper;
	private DroneEventListener deListener;
	private DroneStatusListener dsListener;
	
	private boolean ledToggle = true;
	private DroneStreamer myBlinker;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.connect);
		
		// GUI stuff
		btnFindSensordrone = (ImageButton)findViewById(R.id.btnFindSensordrone);
		btnBack = (ImageButton)findViewById(R.id.btnBack);
		
		// Set button listeners
		btnFindSensordrone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//            	popup();
            	connect();
             }
        });
		
		btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	back();
             }
        });
		
		// Initalize drone
		myDrone = new Drone();
		droneHelper = new DroneConnectionHelper();
		
		// This will Blink our Drone, once a second, Blue
        myBlinker = new DroneStreamer(myDrone, 1000) {
            @Override
            public void repeatableTask() {
                if (ledToggle) {
                myDrone.setLEDs(0, 0, 126);
                } else {
                    myDrone.setLEDs(0,0,0);
                }
                ledToggle = !ledToggle;
            }
        };
		
        // Set up drone listeners
		deListener = new DroneEventListener() {

			@Override
			public void adcMeasured(DroneEventObject arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void altitudeMeasured(DroneEventObject arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void capacitanceMeasured(DroneEventObject arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void connectEvent(DroneEventObject arg0) {
				// Things to do when we connect to a Sensordrone
				quickMessage("Sensordrone Detected!");

                Log.d("chris","FIRMWARE -> " + myDrone.firmwareVersion + "." + myDrone.firmwareRevision);

				if(myDrone.firmwareVersion < 2) {
					wrongFirmwarePopup();
				}
				else {
					DataLogConfiguration.macAddress = myDrone.lastMAC;
					
					myDrone.measureBatteryVoltage();
					
					popup();
				}
			}

			@Override
			public void connectionLostEvent(DroneEventObject arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void customEvent(DroneEventObject arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void disconnectEvent(DroneEventObject arg0) {
//				myBlinker.stop();
			}

			@Override
			public void humidityMeasured(DroneEventObject arg0) {}

			@Override
			public void i2cRead(DroneEventObject arg0) {}

			@Override
			public void irTemperatureMeasured(DroneEventObject arg0) {}

			@Override
			public void oxidizingGasMeasured(DroneEventObject arg0) {}

			@Override
			public void precisionGasMeasured(DroneEventObject arg0) {}

			@Override
			public void pressureMeasured(DroneEventObject arg0) {}

			@Override
			public void reducingGasMeasured(DroneEventObject arg0) {}

			@Override
			public void rgbcMeasured(DroneEventObject arg0) {}

			@Override
			public void temperatureMeasured(DroneEventObject arg0) {}

			@Override
			public void uartRead(DroneEventObject arg0) {}

			@Override
			public void unknown(DroneEventObject arg0) {}

			@Override
			public void usbUartRead(DroneEventObject arg0) {}
		};
		
		dsListener = new DroneStatusListener() {

			@Override
			public void adcStatus(DroneEventObject arg0) {}

			@Override
			public void altitudeStatus(DroneEventObject arg0) {}

			@Override
			public void batteryVoltageStatus(DroneEventObject arg0) {
				DataLogConfiguration.batteryVoltage = myDrone.batteryVoltage_Volts;
			}

			@Override
			public void capacitanceStatus(DroneEventObject arg0) {}

			@Override
			public void chargingStatus(DroneEventObject arg0) {}

			@Override
			public void customStatus(DroneEventObject arg0) {}

			@Override
			public void humidityStatus(DroneEventObject arg0) {}

			@Override
			public void irStatus(DroneEventObject arg0) {}

			@Override
			public void lowBatteryStatus(DroneEventObject arg0) {}

			@Override
			public void oxidizingGasStatus(DroneEventObject arg0) {}

			@Override
			public void precisionGasStatus(DroneEventObject arg0) {}

			@Override
			public void pressureStatus(DroneEventObject arg0) {}

			@Override
			public void reducingGasStatus(DroneEventObject arg0) {}

			@Override
			public void rgbcStatus(DroneEventObject arg0) {}

			@Override
			public void temperatureStatus(DroneEventObject arg0) {}

			@Override
			public void unknownStatus(DroneEventObject arg0) {}
		};
		
		myDrone.registerDroneListener(deListener);
		myDrone.registerDroneListener(dsListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}
	
	private void popup() {
		//AlertDialog.Builder builder = new AlertDialog.Builder(this);
		AlertDialog.Builder builder2 = new AlertDialog.Builder(this);

		 //LayoutInflater inflater = this.getLayoutInflater();
		 //builder.setView(inflater.inflate(R.layout.detecting_sensordrone, null));

		// 3. Get the AlertDialog from create()
		//final AlertDialog dialog1 = builder.create();
		//dialog1.show();
		
		builder2.setMessage(R.string.erase_disclaimer)
		.setPositiveButton("Next", new DialogInterface.OnClickListener() {
               @Override
               public void onClick(DialogInterface dialog, int id) {
            	   resetDLParameters();
            	   
            	   Intent myIntent = new Intent(getApplicationContext(), Profiles.class);
   					startActivity(myIntent);
   					kill();
               }
           })
           .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int id) {
                   myDrone.disconnect();
               }
           });     
		
		
        final AlertDialog dialog2 = builder2.create();
        dialog2.show();
		
//		final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//            	dialog1.dismiss();
//            	
//            }
//        }, 2000);
	}

	private void wrongFirmwarePopup() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setMessage(R.string.firmware_update)
		.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
               @Override
               public void onClick(DialogInterface dialog, int id) {
   					kill();
               }
           })
           .setTitle("Firmware Update Required");
		
        final AlertDialog dialog = builder.create();
        dialog.show();
	}
	
	private void resetDLParameters() {
		for(int i = 0; i < DataLogConfiguration.NUM_SENSORS; i++) {
			DataLogConfiguration.sensorsEnabled[i] = false;
		}
		
		DataLogConfiguration.sampleRate = 0;
		DataLogConfiguration.timeStamp = 0;
		DataLogConfiguration.totalLogs = 0;
		DataLogConfiguration.totalTime = 0;
	}
	
	
	private void back() {
		this.finish();
	}
	
	private void kill() {
		this.finish();
	}
	
	private void next() {
		Intent myIntent = new Intent(getApplicationContext(), Profiles.class);
		startActivity(myIntent);
		this.finish();
	}
	
	public void connect() {
//		droneHelper.scanToConnect(myDrone, this, this, true);
		droneHelper.connectFromPairedDevices(myDrone, this);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		doOnDisconnect();	
		
		myDrone.unregisterDroneListener(deListener);
        myDrone.unregisterDroneListener(dsListener);
	}
	
	/**
	 * Things to do when drone is disconnected
	 */
	public void doOnDisconnect() {

		// Shut off any sensors that are on
		this.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				// Only try and disconnect if already connected
				if (myDrone.isConnected) {
					myDrone.disconnect();
				}
			}
		});
	}
	
	public void quickMessage(final String msg) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT)
						.show();
			}
		});
	}
}
