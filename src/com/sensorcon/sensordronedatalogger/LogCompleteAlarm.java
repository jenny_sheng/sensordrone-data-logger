package com.sensorcon.sensordronedatalogger;

import java.util.concurrent.ExecutionException;

import com.sensorcon.dataloggerutilities.DataLogConfiguration;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

public class LogCompleteAlarm extends BroadcastReceiver {
	
	AlarmManager myAlarmManager;
	private NotificationManager notifier;
	android.support.v4.app.NotificationCompat.Builder notifyLogComplete;
	
	@SuppressLint("Wakelock")
	@Override
	public void onReceive(Context context, Intent intent) {
		
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();
        
        notifier = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		PendingIntent emptyIntent = PendingIntent.getActivity(context, 0, new Intent(), Intent.FLAG_ACTIVITY_NEW_TASK);
		
		Log.d("chris","ON RECEIVE CALLED");
		
		notifyLogComplete = new android.support.v4.app.NotificationCompat.Builder(context);
		notifyLogComplete.setContentTitle("Sensordrone Log Complete!");
		notifyLogComplete.setContentIntent(emptyIntent);
		notifyLogComplete.setSmallIcon(R.drawable.ic_launcher);
		
		notifier.notify(0, notifyLogComplete.build());

        wl.release();
	}
	
	public void setAlarm(Context context) {
        
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, LogCompleteAlarm.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        int minutes =(int)DataLogConfiguration.totalTime;
        am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,SystemClock.elapsedRealtime() + (minutes * 60 * 1000), pi);
        //am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), minutes, pi); // Millisec * Second * Minute                
	}

	public void CancelAlarm(Context context) {
        Intent intent = new Intent(context, LogCompleteAlarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
	}
}
