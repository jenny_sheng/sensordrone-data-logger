package com.sensorcon.sensordronedatalogger;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.sensorcon.dataloggerutilities.DataLogConfiguration;
import com.sensorcon.dataloggerutilities.DatabaseHelper;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.DroneEventObject;
import com.sensorcon.sensordrone.android.Drone;
import com.sensorcon.sensordrone.android.tools.DroneConnectionHelper;

import java.nio.ByteBuffer;

/**
 * Sets up parameters for downloading data
 * 
 * @author Sensorcon, Inc.
 * @version 1.0.0 
 */
public class Download extends OrmLiteBaseActivity<DatabaseHelper> {
	
	private ImageButton btnFindSensordrone;
	private ImageButton btnBack;
	
	private Drone myDrone;
	private DroneConnectionHelper droneHelper;

    private DroneEventHandler droneHandler;

	Intent downloadService;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.connect);
		
		btnFindSensordrone = (ImageButton)findViewById(R.id.btnFindSensordrone);
		btnBack = (ImageButton)findViewById(R.id.btnBack);
		
		downloadService = new Intent(getApplicationContext(), DownloadService.class);
		
		btnFindSensordrone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	connect();
             }
        });
		
		btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	back();
             }
        });
		
		// Initalize drone
		myDrone = new Drone();
		droneHelper = new DroneConnectionHelper();
		
		resetDLParameters();
		
        // Set up drone listeners

        droneHandler = new DroneEventHandler() {
            @Override
            public void parseEvent(DroneEventObject droneEventObject) {

                if (droneEventObject.matches(DroneEventObject.droneEventType.CONNECTED)) {
                    quickMessage("Sensordrone Detected!");

                    Log.d("chris","FIRMWARE -> " + myDrone.firmwareVersion + "." + myDrone.firmwareRevision);

                    if(myDrone.firmwareVersion < 2) {
                        wrongFirmwarePopup();
                    }
                    else {
                        // Store MAC
                        DataLogConfiguration.macAddress = myDrone.lastMAC;
                        myDrone.getDataLoggerInitializationData();
                    }
                }
                else if (droneEventObject.matches(DroneEventObject.droneEventType.DATA_LOGGER_INIT_DATA_READ)) {

                    parseInitData(myDrone.getDataLogInitPacket());

                    myDrone.disconnect();


                }
                else if (droneEventObject.matches(DroneEventObject.droneEventType.DISCONNECTED)) {
                    // Start download
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Download.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            downloadData();
                        }
                    });
                }


            }
        };

		myDrone.registerDroneListener(droneHandler);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}

    private void wrongFirmwarePopup() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(R.string.firmware_update)
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        kill();
                    }
                })
                .setTitle("Firmware Update Required");

        final AlertDialog dialog = builder.create();
        dialog.show();
    }
	
	/**
	 * Parses log initialization data stored in drone
	 */
	private void parseInitData(byte[] packet) {

		byte[] sensors = new byte[2];
		sensors[0] = packet[1];
		sensors[1] = packet[2];
		
		Log.d("chris","sensors: " + sensors[0] + " " + sensors[1]);
		
		byte[] rate = new byte[2];
		rate[0] = packet[3];
		rate[1] = packet[4];
		
		byte[] time = new byte[4];
		time[0] = packet[5];
		time[1] = packet[6];
		time[2] = packet[7];
		time[3] = packet[8];
		
		byte[] count = new byte[4];
		count[0] = packet[9];
		count[1] = packet[10];
		count[2] = packet[11];
		count[3] = packet[12];
		
		if((sensors[1] & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[0] = true;
		}
		if(((sensors[1] >> 1) & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[1] = true;
		}
		if(((sensors[1] >> 2) & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[2] = true;
		}
		if(((sensors[1] >> 3) & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[3] = true;
		}
		if(((sensors[1] >> 4) & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[4] = true;
		}
		if(((sensors[1] >> 5) & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[5] = true;
		}
		if(((sensors[1] >> 6) & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[6] = true;
		}
		if(((sensors[1] >> 7) & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[7] = true;
		}
		if((sensors[0] & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[8] = true;
		}
		if(((sensors[0] >> 1) & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[9] = true;
		}
		if(((sensors[0] >> 2) & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[10] = true;
		}
		if(((sensors[0] >> 3) & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[11] = true;
		}
		if(((sensors[0] >> 4) & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[12] = true;
		}
		if(((sensors[0] >> 5) & (byte)0x01) == 1) {
			DataLogConfiguration.sensorsEnabled[13] = true;
		}
		
		int sampleRate = (((int)rate[0] << 8) & 0xFF00) + rate[1];
		long timeStamp = (long)ByteBuffer.wrap(time).getInt();
		int logCount = ByteBuffer.wrap(count).getInt();
		
		DataLogConfiguration.sampleRate = sampleRate;
		DataLogConfiguration.timeStamp = timeStamp;
		DataLogConfiguration.totalLogs = logCount;
		
		Log.d("chris","Interval: " + String.valueOf(sampleRate));
		Log.d("chris","Log count: " + String.valueOf(logCount));
		Log.d("chris","Timestamp " + String.valueOf(timeStamp));
				
	}
	
	/**
	 * Starts the download
	 */
	private void downloadData() {

		// Start service
        stopService(downloadService);
        startService(downloadService);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.background_info);
		builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                kill();
            }
        });
        
        final AlertDialog dialog = builder.create();
        dialog.show();

	}
	
	/**
	 * Resets all stored datalog parameters
	 */
	private void resetDLParameters() {
		for(int i = 0; i < DataLogConfiguration.NUM_SENSORS; i++) {
			DataLogConfiguration.sensorsEnabled[i] = false;
		}
		
		DataLogConfiguration.sampleRate = 0;
		DataLogConfiguration.timeStamp = 0;
		DataLogConfiguration.totalLogs = 0;
		DataLogConfiguration.totalTime = 0;
	}
	
	/**
	 * Kills activity when back button pressed
	 */
	private void back() {
		this.finish();
	}
	
	/**
	 * Kills activity
	 */
	private void kill() {
		this.finish();
	}
	
	/**
	 * Connects to sensordrone
	 */
	public void connect() {
//		droneHelper.scanToConnect(myDrone, this, this, true);
		droneHelper.connectFromPairedDevices(myDrone, this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		doOnDisconnect();	

        myDrone.unregisterDroneListener(droneHandler);
	}
	
	/**
	 * Things to do when drone is disconnected
	 */
	public void doOnDisconnect() {

		// Shut off any sensors that are on
		this.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				// Only try and disconnect if already connected
				if (myDrone.isConnected) {
					myDrone.disconnect();
				}
			}
		});
	}
	
	public void quickMessage(final String msg) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT)
						.show();
			}
		});
	}
}
