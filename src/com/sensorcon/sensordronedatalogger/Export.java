package com.sensorcon.sensordronedatalogger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.sensorcon.dataloggerutilities.DataLogConfiguration;
import com.sensorcon.dataloggerutilities.DatabaseEntry;
import com.sensorcon.dataloggerutilities.DatabaseHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

/**
 * Exports logs to csv
 * 
 * @author Sensorcon, Inc.
 * @version 1.0.0 
 */
public class Export extends OrmLiteBaseActivity<DatabaseHelper> {
	
	SharedPreferences myPreferences;
    Editor prefEditor;

	private ProgressDialog progressBar;
	private ProgressDialog progressBarQuery;
	private int progressBarStatus = 0;
	private Handler progressBarHandler = new Handler();
	
	private File csv = null;
	private File root = Environment.getExternalStorageDirectory();	
	private FileOutputStream out;
	
	RuntimeExceptionDao<DatabaseEntry, Integer> myDao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.export);
	
		myPreferences = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
		prefEditor = myPreferences.edit();
		
		boolean newData = myPreferences.getBoolean("NEW_DATA", true);
		
		File file = new File (root.getAbsolutePath() + "/Sensordrone_Data_Log");
			
//		if(true) {
		//if(newData == true) {
			prefEditor.putBoolean("NEW_DATA", false);
			prefEditor.commit();
			generateCSV();
//		}
//		else if(!file.exists()) {
//			prefEditor.putBoolean("NEW_DATA", false);
//			prefEditor.commit();
//			generateCSV();
//		}
//		else {
//			csv = new File(file, "Sensordrone_Data_Log.csv");
//			send();
//		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}

	/**
	 * Creates csv file
	 */
	private void generateCSV() {

		progressBar = new ProgressDialog(this);
		progressBarQuery = new ProgressDialog(this);
		
		progressBarQuery.setCancelable(false);
		progressBarQuery.setMessage("Querying Database... (can take up to a minute for long logs)");
		progressBarQuery.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressBarQuery.setIndeterminate(true);
		
		progressBar.setCancelable(false);
		progressBar.setMessage("Generating CSV file...");
		progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressBar.setIndeterminate(false);
		progressBar.setProgress(0);
		
		progressBar.show();
		progressBarQuery.show();

		new Thread(new Runnable() {
			public void run() {

				List<DatabaseEntry> list = null;
				
				try {
					list = getHelper().getEntryDao().queryForAll();
				} catch (SQLException e1) {
					Log.d("chris","DATABASE ERROR -> " + e1.getMessage());
				}
				
				progressBarQuery.dismiss();
				
				progressBar.setMax(list.size());

				// Create CSV
				if(root.canWrite()) {
					
					File dir = new File (root.getAbsolutePath() + "/Sensordrone_Data_Log");
					dir.mkdirs();
					csv = new File(dir, "Sensordrone_Data_Log.csv");

					try {
						out = new FileOutputStream(csv);

						String line;
						String NO_VAL = "";
						long timeStamp = 0;
						String[] vals = new String[DataLogConfiguration.NUM_SENSORS];
						line = "TIME,SENSORDRONE,AMBIENT TEMPERATURE,HUMIDITY,PRESSURE,PRECISION GAS,REDUCING GAS,OXIDIZING GAS,INFRARED TEMPERATURE,CAPACITANCE,EXTERNAL ADC,RED CHANNEL,GREEN CHANNEL,BLUE CHANNEL,CLEAR CHANNEL\n";

						// Header
						out.write(line.getBytes());
						line = "";

						boolean firstLine = true;
						for(DatabaseEntry entry : list) {

							if(entry.getTimeStamp() != timeStamp) {

								if(!firstLine) {
									for(int i = 0; i < DataLogConfiguration.NUM_SENSORS; i++) {
										if(vals[i] == "-99") {
											line += NO_VAL + ","; 
										}
										else {
											
											line += vals[i] + ",";
										}
									}

									line += "\n";
									out.write(line.getBytes());
									line = "";
								}
								else {
									firstLine = false;
								}

								for(int i = 0; i < DataLogConfiguration.NUM_SENSORS; i++) {
									vals[i] = "-99";
								}

								timeStamp = entry.getTimeStamp();
								line = entry.getTimeStamp() + ",";
                                line += entry.getMAC() + ",";
							}
							
							// Check what sensor was read in
							if(entry.getSensor() == SensorID.AMBIENT_TEMPERATURE) {
								vals[0] = String.format("%.1f",entry.getValue());
							}
							else if(entry.getSensor() == SensorID.HUMIDITY) {
								vals[1] = String.format("%.1f",entry.getValue());
							}
							else if(entry.getSensor() == SensorID.PRESSURE) {
								vals[2] = String.valueOf((int)entry.getValue());
							}
							else if(entry.getSensor() == SensorID.PRECISION_GAS) {
								vals[3] = String.format("%.1f",entry.getValue());
							}
							else if(entry.getSensor() == SensorID.REDUCING_GAS) {
								vals[4] = String.valueOf((int)entry.getValue());
							}
							else if(entry.getSensor() == SensorID.OXIDIZING_GAS) {
								vals[5] = String.valueOf((int)entry.getValue());
							}
							else if(entry.getSensor() == SensorID.IR_TEMPERATURE) {
								vals[6] = String.format("%.1f",entry.getValue());
							}
							else if(entry.getSensor() == SensorID.CAPACITANCE) {
								vals[7] = String.valueOf((int)entry.getValue());
							}
							else if(entry.getSensor() == SensorID.EXTERNAL_ADC) {
								vals[8] = String.format("%.3f",entry.getValue());
							}
							else if(entry.getSensor() == SensorID.RED_CHANNEL) {
								vals[9] = String.valueOf((int)entry.getValue());
							}
							else if(entry.getSensor() == SensorID.GREEN_CHANNEL) {
								vals[10] = String.valueOf((int)entry.getValue());
							}
							else if(entry.getSensor() == SensorID.BLUE_CHANNEL) {
								vals[11] = String.valueOf((int)entry.getValue());
							}
							else if(entry.getSensor() == SensorID.CLEAR_CHANNEL) {
								vals[12] = String.valueOf((int)entry.getValue());
							}
							else {
								Log.d("chris","DATABASE ERROR -> " + entry.getSensor());
							}

							progressBarStatus++;
							
							if((progressBarStatus % 50) == 0) {
								progressBarHandler.post(new Runnable() {
									public void run() {
										progressBar.setProgress(progressBarStatus);
									}
								});
							}
						}

						// Create last line!
						for(int i = 0; i < DataLogConfiguration.NUM_SENSORS; i++) {
							if(vals[i] == "-99") {
								line += NO_VAL + ","; 
							}
							else {
								line += String.valueOf(vals[i]) + ",";
							}
						}

						line += "\n";
						out.write(line.getBytes());

						out.close();
						progressBar.dismiss();
						
						send();

					} catch (IOException e) {
						Log.d("chris",e.getMessage());
					}
				}
			}
		}).start();
	}

	/**
	 * Kill activity
	 */
	private void kill() {
		this.finish();
	}
	
	/**
	 * Sends csv to computer
	 */
	private void send() {
		Uri fileUri = Uri.fromFile(csv);
		
		//Uri fileUri = Uri.fromFile(file);
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Sensordrone Log Data");
        sendIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
//        sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(FileProvider.CONTENT_URI + "Sensordrone_Data_Log.csv"));
        sendIntent.setType("text/html");
        startActivity(sendIntent);
        kill();
	}
}
