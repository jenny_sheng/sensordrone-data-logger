package com.sensorcon.sensordronedatalogger;

public class SensorID {
	
	public static final int AMBIENT_TEMPERATURE = 0;
	public static final int HUMIDITY = 1;
	public static final int PRESSURE = 2;
	public static final int PRECISION_GAS = 3;
	public static final int REDUCING_GAS = 4;
	public static final int OXIDIZING_GAS = 5;
	public static final int IR_TEMPERATURE = 6;
	public static final int CAPACITANCE = 7;
	public static final int EXTERNAL_ADC = 8;
	public static final int RED_CHANNEL = 9;
	public static final int GREEN_CHANNEL = 10;
	public static final int BLUE_CHANNEL = 11;
	public static final int CLEAR_CHANNEL = 12;

}
