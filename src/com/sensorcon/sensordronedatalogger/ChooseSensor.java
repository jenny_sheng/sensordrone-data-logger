package com.sensorcon.sensordronedatalogger;

import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;
import com.sensorcon.dataloggerutilities.DatabaseEntry;
import com.sensorcon.dataloggerutilities.DatabaseHelper;
import com.sensorcon.dataloggerutilities.ViewConfiguration;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class ChooseSensor extends Activity {
	
	ListView sensorList;
	SensorAdapter sensorAdapter;
	private ImageButton btnBack;
	
	private ProgressDialog progressBarQuery;
	Dao<DatabaseEntry,Integer> dao;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_sensor);
		
		final DatabaseHelper helper = new DatabaseHelper(this);
		 progressBarQuery = new ProgressDialog(this);
		 progressBarQuery.setCancelable(false);
		  progressBarQuery.setMessage("Querying Database...");
		  progressBarQuery.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		  progressBarQuery.setIndeterminate(true);
		
		btnBack = (ImageButton)findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	back();
             }
        });
	
		String[] sensors = {"Ambient Temperature","Humidity","Pressure","Precision Gas","Reducing Gas","Oxidizing Gas","IR Temperature","Capacitance","External ADC","Red Color","Green Color","Blue Color","Clear Color"};
		
		sensorAdapter = new SensorAdapter(getApplicationContext(), sensors);
	
		sensorList = (ListView)findViewById(R.id.sensorList);
		//droneList.setBackgroundColor(Color.BLACK);
		sensorList.setAdapter(sensorAdapter);
		
		sensorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

		      @Override
		      public void onItemClick(AdapterView<?> parent, final View view,
		    		  int position, long id) {
		    	  ViewConfiguration.sensorID = position;
		    	  progressBarQuery.show();

		    	  new Thread(new Runnable() {
		    		  public void run() {
		    			  try {
		    				  dao = helper.getEntryDao();

		    				  ViewConfiguration.queryList = dao.query(dao.queryBuilder().where().eq("MAC", ViewConfiguration.droneMAC).and().eq("sensor", ViewConfiguration.sensorID).prepare());

		    				  progressBarQuery.dismiss();
		    				  
		    				  Intent myIntent = new Intent(getApplicationContext(), EntryList.class);
		    				  startActivity(myIntent);
		    			  } catch (SQLException e) {
		    				  e.printStackTrace();
		    			  }
		    		  }
		    	  }).start();
		      }

		    });
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}
	
	private void back() {
		this.finish();
	}
	
	public class SensorAdapter extends ArrayAdapter<String> {
		
		private final String[] sensorList;
		private final Context context;
		
		public SensorAdapter(Context context, String[] sensorArray) {
			super(context, R.layout.drone_row, sensorArray);
			this.sensorList = sensorArray;
			this.context = context;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.drone_row, parent, false);
			TextView rowTV = (TextView)rowView.findViewById(R.id.tvDrone);
			rowTV.setBackgroundResource(R.drawable.rounded);
			rowTV.setTextColor(Color.BLACK);
			rowTV.setTextSize(15);
			
			
			String sensor = sensorList[position];

			rowTV.setText(sensor);
			
			return rowView;
		}
		
	}
}
