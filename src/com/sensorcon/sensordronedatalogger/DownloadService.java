package com.sensorcon.sensordronedatalogger;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;
import com.j256.ormlite.dao.Dao;
import com.sensorcon.dataloggerutilities.DataLogConfiguration;
import com.sensorcon.dataloggerutilities.DatabaseEntry;
import com.sensorcon.dataloggerutilities.DatabaseHelper;
import com.sensorcon.dataloggerutilities.MACEntry;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.DroneEventObject;
import com.sensorcon.sensordrone.android.Drone;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Service to download log data in background
 *
 * @author Sensorcon, Inc.
 * @version 1.0.0
 */
public class DownloadService extends Service {

    private int api;
    private final int NEW_API = 0;
    private final int OLD_API = 1;

    final int WAKELOCK_TIMEOUT = 15000;

    byte[] request = {0x50, 0x06, 0x52, 0x00, 0x00, 0x00, 0x00, 0x00};

    DatabaseEntry[] allTheData;

    SharedPreferences myPreferences;
    Editor prefEditor;

    NotificationManager notifier;
    Notification notifyDownloadOld;
    android.support.v4.app.NotificationCompat.Builder notifyDownload;
    android.support.v4.app.NotificationCompat.Builder notifyConnectionLost;
    ProgressDialog progressBar;

    Drone myDrone;
    DatabaseHelper db;

    PowerManager pm;
    PowerManager.WakeLock wl;

    ArrayList<byte[]> entryList = new ArrayList<byte[]>();
    byte[] packet;
    byte[] initPacket;

    private DroneEventHandler droneHandler;


    // IR temperature vars
    private double a1 = 1.75E-3;
    private double a2 = -1.678E-5;
    private double T_REF = 298.15;
    private double b0 = -2.94E-5;
    private double b1 = -5.7E-7;
    private double b2 = 4.63E-9;
    private double c2 = 13.4;
    private double s0 = 2.51E-14;

    // CO vars
    float sensitivity = 0;
    float baseline = 0;
    protected final int gainRes[] =
            {
                    2200000,
                    301961,
                    113793,
                    34452,
                    13911,
                    6978,
                    3494,
                    2747
            };

    // Download vars
    private final DatabaseHelper helper = new DatabaseHelper(this);
    private Dao<DatabaseEntry,Integer> dao;
    private Dao<MACEntry,Integer> macDao;
    private final int NUM_LOGS_TO_READ = 4;
    private int[] sensor = new int[NUM_LOGS_TO_READ];
    private float[] value = new float[NUM_LOGS_TO_READ];
    private long timeStamp;
    private String macAddress;
    private int sampleRate;
    private int numSensors;
    private int sensorCount;
    private int progressBarStatus;
    private long addr = 256;
    private int numLogs;
    private Handler handler = new Handler();
    private int nfails;
    private int nLogsRead;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        wl.acquire();
        myDrone.btConnect(DataLogConfiguration.macAddress);

        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Check to see if API supports progress notification
        if (android.os.Build.VERSION.SDK_INT < 13) {
            api = OLD_API;
        } else {
            api = NEW_API;
        }

        myPreferences = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        prefEditor = myPreferences.edit();

        myDrone = new Drone();

        notifier = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        // If we don't want to do anything, then we need an empty Intent for some versions of Android
        PendingIntent emptyIntent = PendingIntent.getActivity(
                this,
                0,
                new Intent(),  //Dummy Intent do nothing
                Intent.FLAG_ACTIVITY_NEW_TASK);

        // Only newer android apis support progress notification
        if(api == NEW_API) {
            notifyDownload = new android.support.v4.app.NotificationCompat.Builder(this);
            notifyDownload.setContentTitle("Downloading Log Data");
            notifyDownload.setContentIntent(emptyIntent);
            notifyDownload.setSmallIcon(R.drawable.ic_launcher);
        }
        else {
            notifyDownloadOld = new Notification(R.drawable.ic_launcher,"Downloading",System.currentTimeMillis());

            CharSequence title = "Downloading Log Data...";
            RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.noti);
            contentView.setImageViewResource(R.id.status_icon, R.drawable.ic_launcher);
            contentView.setTextViewText(R.id.status_text, title);
            contentView.setProgressBar(R.id.status_progress, 100, 0, false);
            notifyDownloadOld.contentView = contentView;
            notifyDownloadOld.contentIntent = emptyIntent;
        }

        // Connection lost notification
        notifyConnectionLost = new android.support.v4.app.NotificationCompat.Builder(this);
        notifyConnectionLost.setContentTitle("Download Failed - Connection Lost");
        notifyConnectionLost.setContentIntent(emptyIntent);
        notifyConnectionLost.setSmallIcon(R.drawable.ic_launcher);

        pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        db = new DatabaseHelper(getApplicationContext());

        droneHandler = new DroneEventHandler() {
            @Override
            public void parseEvent(DroneEventObject droneEventObject) {

                if (droneEventObject.matches(DroneEventObject.droneEventType.CONNECTED)) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // Store CO vars
                    baseline = myDrone.getPrecisionGasCalibratedBaseline();
                    sensitivity = myDrone.getPrecisionGasCalibratedSensitivity();

                    try {
                        boolean isDuplicate = configureDataLog();
                        if (!isDuplicate) {
                            updateProgressBar();
                            myDrone.readDataLog(request);
                        }
                        else {
                            notifyAlreadyDownloaded();
                            myDrone.disconnect();
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
//                    download();
                }
                else if (droneEventObject.matches(DroneEventObject.droneEventType.CONNECTION_LOST)) {
                    // If connection is lost, notify user
                    notifier.notify(2, notifyConnectionLost.build());
                    if (wl.isHeld()) {
                        wl.release();
                    }
                    //kill();
                }
                else if (droneEventObject.matches(DroneEventObject.droneEventType.DISCONNECTED)) {
                    if (wl.isHeld()) {
                        wl.release();
                    }
                }
                else if (droneEventObject.matches(DroneEventObject.droneEventType.DATA_LOGGER_LOG_READ)) {
                    // Parse the data log
                    try {
                        // -1 means error
                        // 0 means all done
                        // 1 means more to read
                        int result = parseDataRecord(myDrone.dataLogBuffer.array());

                        if (result == -1) {
                            nfails++;

                            if (nfails > 50) {
                                notifyDownloadFailure();
                                myDrone.disconnect();
                            } else {
                                // re-read
                                myDrone.readDataLog(request);
                            }

                        }
                        else if (result == 0) {

                            // Load the data into the database
                            updateDB();

                            myDrone.disconnect();

                        }
                        else {
                            // Assume it was 1
                            nfails = 0;
                            if (progressBarStatus % 48 == 0) {
                                updateProgressBar();
                            }
                            // Take another measurement
                            if (progressBarStatus >= numLogs) {

                                // Load the data into the database
                            	updateDB();

                                myDrone.disconnect();
                            }
                            else {
                                myDrone.readDataLog(request);
                            }

                        }



                    } catch (Exception e) {
                        //
                    }
                }
            }
        };

        myDrone.registerDroneListener(droneHandler);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (wl.isHeld()) {
            wl.release();
        }

        myDrone.disconnect();
        myDrone.unregisterDroneListener(droneHandler);
    }

    public boolean configureDataLog() throws SQLException {
        boolean isDuplicateLog = false;

        // Determine which/how many sensors are enabled
        int numberSensorsEnabled = 0;
        for(int i = 0; i < DataLogConfiguration.NUM_SENSORS; i++) {
            if(DataLogConfiguration.sensorsEnabled[i] == true) {
                numberSensorsEnabled++;
            }
        }
        numLogs = (int) (numberSensorsEnabled * DataLogConfiguration.totalLogs);

        // This can be HUGE
        // But seems to work :-)
        allTheData = new DatabaseEntry[numLogs];

        // determine timestamp
        timeStamp = DataLogConfiguration.timeStamp;
        sampleRate = DataLogConfiguration.sampleRate;


        // check for duplicates

        dao = helper.getEntryDao();
        macDao = helper.getMACDao();

        // Add mac to database
        macAddress = myDrone.lastMAC;
        MACEntry macEntry = new MACEntry(macAddress);

        // Only add mac to db if it is not already there
        List<MACEntry> macList = macDao.query(macDao.queryBuilder().where().eq("MAC", macAddress).prepare());
        if(macList.size() == 0) {
            macDao.createIfNotExists(macEntry);
        }

        // Check for duplicate log
        List<DatabaseEntry> list = dao.query(dao.queryBuilder().where().eq("timeStamp", timeStamp).prepare());
        if(list.size() > 0) {
            isDuplicateLog = true;
        }
        else {
            prefEditor.putBoolean("NEW_DATA", true);
            prefEditor.commit();
        }


        // Initialize some stuff
        numSensors = 0;
        for(int i = 0; i < DataLogConfiguration.NUM_SENSORS; i++) {
            if(DataLogConfiguration.sensorsEnabled[i] == true) {
                numSensors++;
            }
        }
        sensorCount = 0; // Start this at 0!
        progressBarStatus = 0;
        nfails = 0;
        nLogsRead = 0;

        // Initialize the Request packet
        adjustDataLogParameters(256);

        return isDuplicateLog;
    }

    public int parseDataRecord(byte[] logRecord) throws SQLException {
    	
//    	Log.d("chris","Packet -> " + Integer.toHexString(logRecord[0]) + "-" + Integer.toHexString(logRecord[1]) + "-" + Integer.toHexString(logRecord[2]) + "-" + Integer.toHexString(logRecord[3]) + "-" + Integer.toHexString(logRecord[4]) + "-" + Integer.toHexString(logRecord[5]) + "-" + Integer.toHexString(logRecord[6]) + "-" + Integer.toHexString(logRecord[7]) + "-" + Integer.toHexString(logRecord[8]) + "-" + Integer.toHexString(logRecord[9]) + "-" + Integer.toHexString(logRecord[10]) + "-" + Integer.toHexString(logRecord[11]) + "-" + Integer.toHexString(logRecord[12]) + "-" + Integer.toHexString(logRecord[13]) + "-" + Integer.toHexString(logRecord[14]) + "-" + Integer.toHexString(logRecord[15]) + "-" + Integer.toHexString(logRecord[16]) + "-" + Integer.toHexString(logRecord[17]) + "-" + Integer.toHexString(logRecord[18]) + "-" + Integer.toHexString(logRecord[19]) + "-" + Integer.toHexString(logRecord[20]) + "-" + Integer.toHexString(logRecord[21]) + "-" + Integer.toHexString(logRecord[22]) + "-" + Integer.toHexString(logRecord[23]) + "-" + Integer.toHexString(logRecord[24]) + "-" + Integer.toHexString(logRecord[25]) + "-" + Integer.toHexString(logRecord[26]) + "-" + Integer.toHexString(logRecord[27]) + "-" + Integer.toHexString(logRecord[28]) + "-" + Integer.toHexString(logRecord[29]) + "-" + Integer.toHexString(logRecord[30]) + "-" + Integer.toHexString(logRecord[31]));

        // logRecord is 32 bytes.
        // This is 4 log records of 8 bytes each
        byte[][] loggedData = new byte[4][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 4; j++) {
                loggedData[j][i] = logRecord[i + (8*j)];
            }
        }

        // Confirm checksum
        int numberOfZeroChecksums = 0;
        boolean shouldParseLog[] = new boolean[4];
        for (int i = 0; i < 4; i++) {
            int calulatedChecksum = 0;
            for (int j = 0; j < 6; j++){
                calulatedChecksum += unsignedToBytes(loggedData[i][j]);
            }

            int receivedChecksum = ((unsignedToBytes(loggedData[i][6]) << 8) & 0xff00) +
                    ((unsignedToBytes(loggedData[i][7])) & 0x00ff);

            if (calulatedChecksum != receivedChecksum) {
                // If we get a bad checksum, we will need to re-read the whole thing
            	if(receivedChecksum != 65535) {
                	return -1;
            	}
            }

            if (receivedChecksum == 0)  {
                numberOfZeroChecksums++;
            }

            // If we ended mid-record, we will use this to process data and signal that
            // we're finished
            if (receivedChecksum == 65535) {
                shouldParseLog[i] = false;
            }
            else {
                shouldParseLog[i] = true;
            }

        }

        if (numberOfZeroChecksums == 4) {
            // We read all zeroes; this means we're done
            return 0;
        }

        // Parse the data now that we know we have good checksums

        for (int i = 0; i < 4; i++) {
            if (shouldParseLog[i]) {
                int sensor = loggedData[i][1];
                byte[] rawData = {loggedData[i][2], loggedData[i][3], loggedData[i][4], loggedData[i][5]};
                float reading = getSensorValue(sensor, rawData);

                // Store the data
                final DatabaseEntry entry = new DatabaseEntry(timeStamp, sensor, reading, macAddress);
                allTheData[progressBarStatus] = entry;
//                dao.create(entry);
//                UpdateDB updater = new UpdateDB();
//                updater.execute(entry);
                progressBarStatus++;

                sensorCount++;
                if (sensorCount > numSensors - 1) {
                    sensorCount = 0;
                    timeStamp += sampleRate;
                }
            }
            else {
                // This means we're done
                return 0;
            }
        }

        // If we've made it this far, all is good, but there is more data to read
        // Adjust parameter for next run
        adjustDataLogParameters(32);

        return 1;
    }


    /**
     * This should be passed either 8 (next read) or 0 (re-read)
     *
     * Run once at start (with 256) to initialize variable
     * @param addrOffset
     */
    private void adjustDataLogParameters(long addrOffset) {

        if (addrOffset == 256) {
            addr = 256;
        }
        else {
            addr += addrOffset;
        }

        request[3] = (byte)(addr & 0x00000000000000FF);
        request[4] = (byte)((addr >> 8) & 0x00000000000000FF);
        request[5] = (byte)((addr >> 16) & 0x00000000000000FF);
        request[6] = (byte)0x0F;


    }

    public void notifyDownloadFailure() {
        if(api == NEW_API) {
            notifyDownload.setContentText("Download ended early. May be caused by Sensordrone being reset during logging.");
            notifier.notify(1, notifyDownload.build());
        }
        else {
            notifyDownloadOld.contentView.setProgressBar(R.id.status_progress,numLogs, numLogs, false);
            notifyDownloadOld.contentView.setTextViewText(R.id.status_text, "Download ended early. May be caused by Sensordrone being reset during logging.");
            notifier.notify(1, notifyDownloadOld);
        }
    }

    public void notifyDownloadSuccess() {
        if(api == NEW_API) {
            notifyDownload.setProgress(numLogs, numLogs, false);
            notifyDownload.setContentText("Download Complete!");
            notifier.notify(1, notifyDownload.build());
        }
        else {
            notifyDownloadOld.contentView.setProgressBar(R.id.status_progress,numLogs, numLogs, false);
            notifyDownloadOld.contentView.setTextViewText(R.id.status_text, "Download Complete!");
            notifier.notify(1, notifyDownloadOld);
        }
    }
    
    public void notifyAlreadyDownloaded() {
        if(api == NEW_API) {
            notifyDownload.setContentText("This log has already been downloaded.");
            notifier.notify(1, notifyDownload.build());
        }
        else {
            notifyDownloadOld.contentView.setTextViewText(R.id.status_text, "This log has already been downloaded.");
            notifier.notify(1, notifyDownloadOld);
        }
    }

    public void updateProgressBar() {
        if(api == NEW_API) {

            notifyDownload.setProgress(numLogs, progressBarStatus, false);
            notifier.notify(1, notifyDownload.build());
        }
        else {
            notifyDownloadOld.contentView.setProgressBar(R.id.status_progress,numLogs, progressBarStatus, false);
            notifier.notify(1, notifyDownloadOld);
        }
    }

    /**
     * Updates database with downloaded logs
     */
    private void updateDB() {
    	
    	if(api == NEW_API) {
            notifyDownload.setContentTitle("Updating Database");
        }
        else {  
        	RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.noti);
            contentView.setTextViewText(R.id.status_text, "Updating Database");
            notifyDownloadOld.contentView = contentView;
        }
    	
    	new Thread(new Runnable() {	
    		public void run() {
    			
    			try {
                    dao.callBatchTasks(new Callable<Object>() {
                        @Override
                        public Object call() throws Exception {
                            progressBarStatus = 0;

                            // For all the possible logs
                            for (int i = 0; i < numLogs; i++) {
                                // Create if not null
                                if (allTheData[(i)] != null) {
                                    dao.create(allTheData[i]);
                                }

                                if((progressBarStatus % 48) == 0) {
                                    updateProgressBar();
                                }
                                progressBarStatus++;
                            }

                            notifyDownloadSuccess();

                            return null;
                        };
                    });
                } catch (Exception e) { }
    		}
    	}).start();
    }
    
    /**
     * Returns the actual sensor decimal value
     *
     * @param id	Sensor id
     * @param val	Raw data from sensor
     * @return		Decimal value
     */
    private float getSensorValue(int id, byte[] val) {

        float value = 0;

        if(id == 0) {
            int MSB = 0x000000ff & ((int) val[0]); // order is opposite what's expected but gives more stable result
            int LSB = 0x000000fc & ((int) val[1]); // fc not ff
            int ADC = LSB + (MSB << 8);

            float temperature = (float) (-46.85 + 175.72 * ((float) ADC / Math
                    .pow(2, 16)));


            value = (float) (temperature
                    * (9.0 / 5.0) + 32.0);
        }
        else if(id == 1) {
            int MSB = 0x000000ff & ((int) val[0]); // order is opposite what's expected but gives more stable result
            int LSB = 0x000000fc & ((int) val[1]); // fc not ff

            int ADC = LSB + (MSB << 8);

            // RH above water
            value = (float) (-6.0 + 125.0 * ((float)ADC/ Math.pow(2, 16)));
        }
        else if(id == 2) {
            byte[] presByte = { val[0], val[1] };
            BigInteger bigPres = new BigInteger(presByte);
            int presIntBits = 0x000000ff & ((int) val[2] & 0x0c);
            int presDecBits = 0x000000ff & ((int) val[2] & 0x03);
            value = (float) ((bigPres.intValue() << 2)
                    + presIntBits + (presDecBits / 4.0));
        }
        else if(id == 3) {
            int LSB = 0x000000FF & val[0];
            int MSB = 0x000000FF & val[1];
            int gainStage = 0x000000FF & val[2];
            int ADC = (MSB << 8) + LSB;

            // PPM Calculation
            float deltaADC = (float)((float)ADC - baseline);
            float gasResponse = (float) ((deltaADC * 3.0e9) / 4096.0);

            value = (float) (gasResponse / (sensitivity * (float) gainRes[gainStage]));
        }
        else if(id == 4) {
            int redMSB = 0x000000ff & ((int) val[1]);
            int redLSB = 0x000000ff & ((int) val[0]);
            int redADC = redLSB + (redMSB << 8);
            float voltage = (float) ((redADC / 4095.0) * 3.3);

            value = (float) ((270000.0 * 3.3 / voltage) - 270000.0);
        }
        else if(id == 5) {
            int oxMSB = 0x000000ff & ((int) val[1]);
            int oxLSB = 0x000000ff & ((int) val[0]);
            int oxADC = oxLSB + (oxMSB << 8);
            float voltage = (float) (((float) oxADC / 4095.0) * 3.3);

            value = (float) ((18000.0 * 3.3 / voltage) - 18000.0);
        }
        else if(id == 6) {
            // Data is in two's complement so we should be able to use a BigInteger
            byte[] twosC_temp = {val[0], val[1]};
            byte[] twosC_volt = {val[2], val[3]};

            BigInteger bigIntTemp = new BigInteger(twosC_temp);
            BigInteger bigIntVolt = new BigInteger(twosC_volt);

            int T_DIE = bigIntTemp.intValue();
            int V_OBJ = bigIntVolt.intValue();

            // Parse the data
            double dT_Die = (double) ((T_DIE / (32.0 * 4.0)) + 273.15); // Should be Kelvin.
            double dV_Obj = (double) (V_OBJ * 156.25e-9); // Should be in Volts
            double Vos = V_os(dT_Die);
            double sensitivity = S(dT_Die, s0);
            double fVobj = Seebeck(dV_Obj, Vos);
            double TMP = dT_Die * dT_Die * dT_Die * dT_Die
                    + (fVobj / sensitivity);
            double temperature = Math.sqrt(TMP);
            temperature = Math.sqrt(temperature);

            double KNOWN_TEMPERATURE = 273.15;
            double calX = Math.pow(KNOWN_TEMPERATURE, 4) - Math.pow(dT_Die, 4);
            double calY = fVobj / (1 + a1 * (dT_Die - T_REF) + a2 * (dT_Die - T_REF));

            // Assign our values
            float kelvin = (float) temperature;
            float celcius = (float) (kelvin - 273.15);
            float fahrenheit = (float) (celcius
                    * (9.0 / 5.0) + 32.0);

            value = fahrenheit;
        }
        else if(id == 7) {
            int MSB = 0x000000ff & ((int) val[1]);
            int LSB = 0x000000ff & ((int) val[2]);
            int ADC = (MSB << 8) + LSB;

            // *4000 is nF
            value = (float) (((float) ADC / 65520.0) * 4000);
        }
        else if(id == 8) {
            int MSB = 0x000000ff & ((int) val[1]);
            int LSB = 0x000000ff & ((int) val[0]);
            int ADC = LSB + (MSB << 8);
            value = (float) (((float) ADC / 4095.0) * 3.0);
        }
        else if(id == 9) {
            float R = bytes2int(val[1], val[0]);
            double Rcal = 0.2639626007;

            R += R * Rcal;

            value = R;
        }
        else if(id == 10) {
            float G = bytes2int(val[1], val[0]);
            double Gcal = 0.2935368922;

            G += G * Gcal;

            value = G;
        }
        else if(id == 11) {
            float B = bytes2int(val[1], val[0]);
            double Bcal = 0.379682891;

            B += B * Bcal;

            value = B;
        }
        else if(id == 12) {
            float C = bytes2int(val[1], val[0]);
            double Ccal = 0.2053011829;

            C += C * Ccal;

            value = C;
        }
//		else {
//			value = 9999;
//		}

        return value;
    }

    // IR Temperature equations
    private double S(double T_DIE, double calibration_constant) {
        double sensitivity = calibration_constant * (1 + a1 * (T_DIE - T_REF) + a2 * (T_DIE - T_REF) * (T_DIE - T_REF));
        return sensitivity;
    }
    private double V_os(double T_DIE) {
        double offset = b0 + b1 * (T_DIE - T_REF) + b2 * (T_DIE - T_REF) * (T_DIE - T_REF);
        return offset;
    }
    private double Seebeck(double V_Obj, double V_os) {
        double f_V_Obj = (V_Obj - V_os) + c2 * (V_Obj - V_os) * (V_Obj - V_os);
        return f_V_Obj;
    }

    /**
     * A method used to convert an MSB and an LSB to an int
     * @param MSB
     * @param LSB
     * @return
     */
    public int bytes2int(byte MSB, byte LSB) {
        int intMSB = 0x000000ff & ((int) MSB);
        int intLSB = 0x000000ff & ((int) LSB);
        return ((intMSB << 8) + intLSB);
    }

    /**
     * Returns unsigned int
     * @param b	Bytes
     * @return	Unsigned int
     */
    public int unsignedToBytes(byte b) {
        return b & 0xFF;
    }

    /**
     * Ends the service
     */
    private void kill() {
        stopSelf();
    }
}