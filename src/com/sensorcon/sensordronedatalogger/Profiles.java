package com.sensorcon.sensordronedatalogger;

import com.sensorcon.dataloggerutilities.DataLogConfiguration;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

public class Profiles extends Activity {
	
	private ImageButton btnLoadProfile;
	private ImageButton btnNewProfile;
	private ImageButton btnBack;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.logging_profile);
		
		btnLoadProfile = (ImageButton)findViewById(R.id.btnLoadProfile);
		btnNewProfile = (ImageButton)findViewById(R.id.btnNewProfile);
		btnBack = (ImageButton)findViewById(R.id.btnBack);
		
		DataLogConfiguration.usingProfile = false;
		
		btnLoadProfile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent myIntent = new Intent(getApplicationContext(), SavedProfiles.class);
        		startActivity(myIntent);
        		kill();
        		
             }
        });
		
		btnNewProfile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent myIntent = new Intent(getApplicationContext(), ChooseSensors.class);
        		startActivity(myIntent);
        		kill();
             }
        });
		
		btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	back();
             }
        });
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}

	private void back() {
		Intent myIntent = new Intent(getApplicationContext(), Connect.class);
		startActivity(myIntent);
		this.finish();
	}
	
	private void kill() {
		this.finish();
	}
}
