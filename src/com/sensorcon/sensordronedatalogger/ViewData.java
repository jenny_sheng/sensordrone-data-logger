package com.sensorcon.sensordronedatalogger;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class ViewData extends Activity {
	
	private ImageButton btnView;
	private ImageButton btnExport;
	private ImageButton btnDeleteAll;
	private ImageButton btnBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_data);
		
		btnView = (ImageButton)findViewById(R.id.btnView);
		btnExport = (ImageButton)findViewById(R.id.btnExport);
		btnDeleteAll = (ImageButton)findViewById(R.id.btnDeleteAll);
		btnBack = (ImageButton)findViewById(R.id.btnBack);
		
		btnView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent myIntent = new Intent(getApplicationContext(), ChooseSensordrone.class);
				startActivity(myIntent);
             }
        });
		
		btnExport.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent myIntent = new Intent(getApplicationContext(), Export.class);
				startActivity(myIntent);
             }
        });
		
		btnDeleteAll.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				delete();
			}
        });
		
		btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	back();
             }
        });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		
		case R.id.instructions:
			
			Intent myIntent = new Intent(getApplicationContext(), Instructions.class);
			startActivity(myIntent);
			break;
		}
		return true;
	}
	
	private void delete() {
		
		Intent myIntent = new Intent(getApplicationContext(), Delete.class);
		startActivity(myIntent);
//		
//		AlertDialog.Builder builder = new AlertDialog.Builder(this);
//		
//		builder.setMessage(R.string.delete_disclaimer)
//		.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
//               @Override
//               public void onClick(DialogInterface dialog, int id) {
//            	   Intent myIntent = new Intent(getApplicationContext(), Delete.class);
//   					startActivity(myIntent);
//               }
//           })
//           .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//               public void onClick(DialogInterface dialog, int id) {
//                   
//               }
//           }); 
//		
//		final AlertDialog dialog = builder.create();
//		dialog.show();
	}
	
	private void back() {
		this.finish();
	}

}